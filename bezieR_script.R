
# load packages ----

library(Hmisc)
library(sf)
library(tidyverse)

# load functions ----

source("bezieR_functions.R")

# load data ----

spatUnits <- readRDS("DATA/spatunits.Rds")
commutingFlows <- readRDS("DATA/commutingflows.Rds") %>% 
  filter(ORI != DES)

# map flows ----

commutingFlows <- join_coordinates(tabflows = commutingFlows, tabunits = spatUnits, varid = "CODGEO", varori = "ORI", vardes = "DES", varflow = "FLOW")
oneDes <- commutingFlows %>% filter(DES == "44162")
bezierCurves <- make_bezier(tab = oneDes, len = 50, varflow = "FLOW")

ggplot() +
  geom_sf(data = spatUnits, fill = "snow2", color = "white") + 
  geom_path(data = bezierCurves, aes(x = x, y = y, group = Group, colour = SEQ, size = WGT), lineend = "butt", alpha = 0.5) +
  scale_colour_gradient(low = "olivedrab", high = "orangered") +
  theme_void()
  